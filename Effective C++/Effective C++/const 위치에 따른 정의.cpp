#include <iostream>
#include <vector>
#include <iterator>
using namespace std;

int main() {
	vector<int> vec;

	const vector<int>::iterator iter = vec.begin();

	// 반복자가 const 따라서 반복자를 변경하지 못함
	*iter = 10; 
	// ++iter; 에러

	vector<int>::const_iterator cIter = vec.begin();
	
	// 객체의 상수성을 갖고있음 따라서 반복자 변경가능하지만, 객체 변경 불가능
	// *cIter = 10; 에러
	++cIter;
}