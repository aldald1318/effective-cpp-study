#include <iostream>
#include <string>
using namespace std;


class CTextBlock {
public:
	CTextBlock(char* str) {
		pText = str;
	}
	~CTextBlock() = default;

	// &가 없을시 컴파일오류 : 원본이 아님

	// const
	/* const */ char& operator[](size_t position) const {
		return pText[position];
	}

	// non-const
	char& operator[](size_t position) {
		return pText[position];
	}

private:
	char* pText;
};

int main()
{
	char a[6] = "Hello";

	const CTextBlock ctb(a);
	ctb[0] = 'x';

	// 논리적 상수성
	// 비트수준 상수성에서 알지못함.
	// 비트수준 상수성: 구성하는 비트중 어떤것도 바꾸면 안됨
	// 객체가 const지만 값이 바뀌어버림
	cout << ctb[0] << endl;
}