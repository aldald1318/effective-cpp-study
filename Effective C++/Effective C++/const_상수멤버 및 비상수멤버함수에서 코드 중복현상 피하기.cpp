#include <iostream>
#include <string>
using namespace std;


class CTextBlock {
public:
	CTextBlock(char* str) {
		pText = str;
	}
	~CTextBlock() = default;

	// &가 없을시 컴파일오류 : 원본이 아님

	// const
	const char& operator[](size_t position) const {

		// 경계검사
		// 접근 데이터 로깅
		// 자료 무결성 검증
		if (!lengthIsValid) {
			textlength = strlen(pText);
			lengthIsValid = true;
		}

		return pText[position];
	}

	// non-const : 중복코드 제거
	char& operator[](size_t position) {
		
		return
			// const 떼어내기
			const_cast<char&>(

				// const로 형변환 후
				// const 연산자 실행
				static_cast<const CTextBlock&>
				(*this)[position]
				);
	}

private:
	char* pText;

	mutable size_t textlength;
	mutable bool lengthIsValid;
};

int main()
{
	char a[6] = "Hello";

	CTextBlock ctb(a);

	// 논리적 상수성
	// 비트수준 상수성에서 알지못함.
	// 객체가 const지만 값이 바뀌어버림
	ctb[0] = 'x';
	cout << ctb[0] << endl;
}