#include <iostream>
#include <string>
#include <Windows.h>
using namespace std;

class Player {
private:
	string name;
	int id;

public:
	Player(string _name);
	explicit Player(int _id);

	string GetString() const { return name; }
};
Player::Player(string _name) : name(_name), id(-1)
{
}
Player::Player(int _id) : id(_id), name("")
{
}

void DoSomethingStr(Player p)
{
	// 플레이어 이름을 사용한 작업,,,
	cout << p.GetString() << endl;
}

int main()
{
	DoSomethingStr(Player("p1"));

	// 함수 파라미터에서 컴파일러가 자동으로 implicit 변환
	string name = "p1";
	DoSomethingStr(name);

	// 컴파일러가 자동으로 Player(int)로 암시적 변환을 해주어서 컴파일 에러가 발생하지 않음.
	DoSomethingStr(3);
}

