#include <iostream>
using namespace std;

class Investment{
public:
	Investment() {
		cout << "Create" << endl;
	}
	~Investment() {
		cout << "Delete" << endl;
	}

	int data = 5;
};

Investment* CreateInvestment()
{
	return new Investment;
}

int main()
{
	//// 객체를 삭제하기전, return으로 도중하차 할경우, 메모리 누수 발생
	//Investment* pInv = CreateInvestment();
	//// ... return이 있을경우?
	//delete pInv;

	//// 대책
	//// 함수로 얻어낸 자원이 항상 해제하도록 만들 방법은, 자원을 객체에 넣고 그 자원 해제를 소멸자가 맡도록 한다.
	//auto_ptr<Investment> pInv(CreateInvestment());

	//// 복사를 할경우, 복사생성자, 복사대입연산자를 통해 원본 객체는 null로 만든다.
	//// 복사하는 객체만이 자원의 유일한 소유권을 갖는다고 가정
	//auto_ptr<Investment> pInv2(pInv); // pInv는 null, pInv2가 자원의 소유권을 갖고 있음.

	// 복사가 훨씬 자연스러워짐
	shared_ptr<Investment> pInv(CreateInvestment());
	shared_ptr<Investment> pInv2(pInv);

	cout << pInv->data << endl;
	cout << pInv2->data << endl;
}