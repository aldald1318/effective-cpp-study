#include <iostream>
using namespace std;

class Investiment
{
public:
	int data;
};

Investiment* CreateInvestiment()
{
	return new Investiment;
}

int daysHeld(const Investiment* pi)
{
	return 1;
}

int main()
{
	shared_ptr<Investiment> pInv(CreateInvestiment());

	// int days(daysHeld(pInv)); // error
	// 위 에러와 같이 우리는 자원관리 객체로부터 Investment*를 얻고싶다.
	// int days(daysHeld(pInv.get()));
	
	int invData = pInv.get()->data; // explicit : 명시적
	int invData = pInv->data; // implicit : 암시적
}