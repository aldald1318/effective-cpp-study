#include <iostream>
using namespace std;


class TEST {
public:
	static TEST& GetInstance(int pa) {
		static TEST a(pa);
		
		// 예시 2 : 기본타입 객체도 동일함 생성자 1번 호출
		static int test = pa;
		cout << test << endl;

		return a;
	}

private:
	TEST(int pa) : param(pa) { cout << "생성자 호출" << endl; }
	~TEST() { cout << "소멸자 호출" << endl; }

public:
	int param;
};


int main() 
{
	// static 객체의 경우,static객체 호출시 생성자가 딱 한번만 호출됨
	// 소멸은 main문이 끝날경우 모든 static객체 소멸자 호출
	TEST* a = &TEST::GetInstance(3);
	TEST* b = &TEST::GetInstance(5);
	TEST* c = &TEST::GetInstance(6);

	// 결과는 3 3 3
	cout << a->param << endl;
	cout << b->param << endl;
	cout << c->param << endl;
}