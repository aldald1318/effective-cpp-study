#include <iostream>
using namespace std;

// 자동으로 생성되어 호출되는 기본 복사생성자
// 객체가 아닌 포인터를 갖고 있을때, 얕은복사로 인한 문제점발생

template<class T>
class NameObject {
public:
	NameObject(const char* name, const T& value) : nameValue(name), objectValue(value) {}
	NameObject(const string name, const T& value) : nameValue(name), objectValue(value) {}

public:
	string nameValue;
	T objectValue;
};

int main()
{
	NameObject<const char*> no1("Smallest Prime Number", "test");
	NameObject<const char*> no2(no1);		// 복사 생성자 호출
											// 자동으로 생성되는 기본 복사생성자에서, 각 비트만 그대로 복사함 : 얕은복사가 일어남.
	cout << no1.nameValue << endl;
	cout << no2.nameValue << endl;

	delete no1.objectValue;
	cout << no2.objectValue << endl;		// Error 발생 : 기본 복사생성자에서 얕은복사로 인해, no1.objectValue와 no2.objectValue는 같은 주소를 갖게됨
}