#include <iostream>
using namespace std;

// 복사생성자를 암시적(Implicit) 으로 만들어 놓음

template<class T>
class NameObject {
public:
	NameObject(string& name, const T& value) : nameValue(name), objectValue(value) {}

public:
	string& nameValue;
	const T objectValue;
};

int main()
{
	string newDog("Persephone");
	string oldDog("Satch");

	NameObject<int> d1(newDog, 2);
	NameObject<int> d2(oldDog, 2);

	d2 = d1;	// Error : 참조자는 원래 자신이 참조하고 있는것과 다른 객체를 참조할 수 없음. 또한 상수객체도 동일한 에러가 발생
	
	// 따라서 이러한 경우 복사 대입연산자를 직접 정의해 주어야한다.
}