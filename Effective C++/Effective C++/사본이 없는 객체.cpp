#include <iostream>
using namespace std;

class NonCopyable{
public:
	NonCopyable() { cout << "Non 생성자 호출" << endl; };
	~NonCopyable() { cout << "Non 소멸자 호출" << endl; };

protected:
	// 선언만 함으로서 friend를 통한 호출도 막음
	NonCopyable(const NonCopyable&);
	NonCopyable& operator=(const NonCopyable&);
};

// private 상속 : NonCopyable을 기본클래스로 지정하지 못하도록 함.
// 소멸자는 가상소멸자가 아니어도 된다. :  private으로 NonCopyable을 기본클래스로 지정하지 못하도록 막아놓았기 때문에, 쓸데없이 가상 소멸자를 지정할 필요가 없다.
class UniqueObj : private NonCopyable {
public:
	UniqueObj() { cout << "Unique 생성자 호출" << endl; }
	~UniqueObj() { cout << "Unique 소멸자 호출" << endl; }
};

int main()
{
	// private 상속이기 때문에, NonCopyable을 기본베이스로 생성할 수 없음. -> virtual 소멸자도 필요없음
	// NonCopyable* a = new UniqueObj;
	
	UniqueObj a;
	//UniqueObj b;
	//b = a; // compile error
}