#include <iostream>
using namespace std;

class Transaction {
public:
	explicit Transaction(const string& logInfo) {
		logTransaction(logInfo);
	}
	virtual ~Transaction() = default;
	
	/*virtual*/ void logTransaction(const string& logInfo) const{
		cout << "Log : " << logInfo << endl;
	}

};

class BuyTransaction : public Transaction {
private:
	// 미초기화된 데이터 멤버
	int param1;
	int param2;
	int param3;

public:
	// 필요한 초기화 정보를 파생클래스 쪽에서 기본클래스 생성자로 올려주도록 만듬
	BuyTransaction(const string& param) : Transaction(createLogString(param)) {}

private:
	// 중요! 정적함수인 이유
	// 정적함수이기 때문에 생성이 끝나지도 않은 BuyTransaction의 미초기화된 데이터 멤버를 실수로 건드릴 위험도 없다.
	static string createLogString(const string& param) {
		return param;
	}
};

int main()
{

}