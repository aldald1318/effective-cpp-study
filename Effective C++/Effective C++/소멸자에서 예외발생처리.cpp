#include <iostream>
#include <array>
using namespace std;

class DBConnection {
public:
	DBConnection() = default;
	DBConnection(const DBConnection& rhs) = default;

	static DBConnection& create() 
	{
		static DBConnection a;
		return a;
	}

	bool close() 
	{
		//...
		return false;
	}
};

// DBConnection을 관리하는 클래스
//class DBConn {
//public:
//	DBConn(const DBConnection _db) : db(_db) {
//	}
//	~DBConn()
//	{
//		db.close(); // 여기서 예외가 발생할경우?
//					// db는 닫히지 않은채로 객체가 소멸해버림.
//					// 해결방법 : 상황에 따라 선택지 선택
//					// 1. 프로그램을 끝내거나 / 2. 호출 실패 로그 남김
//	}
//
//private:
//	DBConnection db;
//};

class DBConn {
public:
	DBConn(const DBConnection _db) : db(_db), closed(false) {
	}
	~DBConn()
	{
		// 실패했을때를 대비해 예외처리 추가
		if(!closed)
		try
		{
			closed = db.close(); // 사용자가 연결을 안닫았으면 여기서 닫기 시도
			if (!closed) throw closed;
		}
		catch (const bool& e)
		{
			// 1. exit
			//exit(true);
			// 2. close log 출력
			cout << "Exception Log : DB Is not closed" << endl;
		}
	}

	// 사용자 편의 함수
	void close()
	{
		db.close();
		closed = true;
	}

private:
	DBConnection db;
	bool closed;
};

int main() 
{
	DBConn dbc (DBConnection::create());
}