#include <iostream>
using namespace std;

class Resource {

};

class Widget {
private:
	Resource* pb; // 힙 할당한 리소스

public:
	void swap(Widget& rhs){}

public:
	// 안전하지 않은 operator=
	Widget& operator=(const Widget& rhs) {
		// 만약 this = rhs (자기대입)이라면
		// pb는 삭제된 자기 리소스를 복사하게 됨
		delete pb;
		pb = new Resource(*rhs.pb);
		return *this;
	}

	// 대책1 : 일치성검사
	Widget& operator=(const Widget& rhs) {

		// 객체가 같은지, 자기대입인지를 검사한다.
		if (this == &rhs) return *this;

		delete pb;
		pb = new Resource(*rhs.pb);
		return *this;
	}

	// 대책2 : 코드 문맥순서 변경
	Widget& operator=(const Widget& rhs) {

		// 원본포인터를 사본 포인터에 받아놓고, 사본포인터를 통해 원본데이터 삭제
		Resource* orig = pb;
		pb = new Resource(*rhs.pb);
		delete orig;

		return *this;
	}

	// 대책3 : 복사 후 맞바꾸기 (copy and swap)
	// 자기대입안전성 (+ 예외 안전성)
	Widget& operator=(const Widget& rhs) {

		Widget temp(rhs); // 객체 소멸자를 이용한 자원관리 방법
		swap(temp); // + 예외안전성 (자세한 부분은 자원관리파트에서 진행)
		return *this;
	}
};