#include <iostream>
#include <mutex>
#include <Windows.h>
using namespace std;

void lock(mutex* pm)
{
	pm->lock();
}
void unlock(mutex* pm)
{
	pm->unlock();
}

class Lock {
public:
	explicit Lock(mutex* pm) : m_pMutex(pm, unlock){  // shared_ptr이 소멸될때, mutex 객체가 삭제되면 안된다. deleter 대신 unlock함수를 호출하도록 생성자에서 설정
		lock(m_pMutex.get()); // get() : 명시적(explicit) 변환
		cout << "create" << endl;
	}
	~Lock() {
		// m_pLock이 소멸되면서, 자동으로 설정해놓은 unlock함수 호출
		cout << "delete" << endl;
	}

private:
	//mutex* m_pMutex;

	/* 객체 복사의 경우 */
	// 1. 복사 금지, 복사 함수 = delete
	// 2. 참조카운팅을 사용하여, 복사기능추가
	shared_ptr<mutex> m_pMutex; 
};

mutex m;

int main() 
{
	// Critical Section
	{
		// RAII
		Lock m1(&m);
		// 이럴 경우엔?
		Lock m2(m1);
	}

	system("pause");
}