#include <iostream>
using namespace std;

class Customer {
private:
	int name;

public:
	Customer() : name(-1) {}
	virtual ~Customer() = default;

	Customer(const Customer& rhs) : name(rhs.name) {}
	Customer& operator=(const Customer& rhs) {
		name = rhs.name;
		return *this;
	}

	void SetName(const int _name) {
		name = _name;
	}
	const int GetName() const {
		return name;
	}
};

class PriorityCustomer : public Customer {
private:
	int priority;

public:
	PriorityCustomer() : priority(-1) {};
	virtual ~PriorityCustomer() = default;

	// 기본생성자의 데이터복사가 일어나지 않고있음.
	PriorityCustomer(const PriorityCustomer& rhs) : priority(rhs.priority){}
	PriorityCustomer& operator=(const PriorityCustomer& rhs) {
		priority = rhs.priority;
		return *this;
	}

	// 파생클래스 복사뿐만 아니라, 기본생성자까지 복사
	//PriorityCustomer(const PriorityCustomer& rhs) : Customer(rhs), priority(rhs.priority) {}
	//PriorityCustomer& operator=(const PriorityCustomer& rhs) {
	//	Customer::operator=(rhs);
	//	priority = rhs.priority;
	//	return *this;
	//}
};

int main()
{
	PriorityCustomer a;
	a.SetName(5);
	PriorityCustomer b(a);
	cout << b.GetName() << endl; // 5가 나와야하지만 기본값 -1이 나옴
}