#include <iostream>
using namespace std;

// Rational : 모든 매개변수에 대한 타입캐스팅허용
class Rational {
private:
	int m_Numerator;
	int m_Denominator;

public:
	// int -> Rational implicit 변환
	Rational(int numerator = 0, int denominator = 1) : m_Numerator(numerator), m_Denominator(denominator)
	{
	}

	int numerator() const { return m_Numerator; }
	int denominator() const { return m_Denominator; }
};

const Rational& operator*(const Rational& lhs, const Rational& rhs)
{
	return Rational(lhs.numerator() * rhs.numerator(), lhs.denominator() * rhs.denominator());
}

int main()
{
	// 2.operator*(T)를 사용해보자
	// 멤버 연산자가 없네? 비멤버 버전의 operator*(T, T)를 사용하자
	Rational result = 2 * Rational(1,1);
	cout << result.numerator() << endl;
}